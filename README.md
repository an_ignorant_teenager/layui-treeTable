# layui-treeTable

#### 介绍
临时用于同步github layui2.8预览版的treeTable相关代码，方便进行本地测试，layui-v2.8.0正式版发布之后可能会删该仓库。<br>
文档以及问题反馈：[treeTable-v0.1](https://gitee.com/layui/layui/issues/I6V5VY)
